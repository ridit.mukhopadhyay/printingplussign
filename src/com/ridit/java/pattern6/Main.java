package com.ridit.java.pattern6;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the length of the square. it should be odd");
		int length = in.nextInt();
		for(int row = 0;row<length;row++) {
			if( row == length / 2){
				printMiddleRow(length);
			}
			else {
				printRow(row, length);
			}	
		}	
	}
	static void printRow(int rownumber, int length) {
		for(int column = 0;column<length;column++) {
			if(column == length / 2) {
				System.out.print("*" + "\t");
			}
			else {
				System.out.print("\t");
			}
		
		}
		System.out.println();
		System.out.println();
		
	}
	
	static void printMiddleRow(int length) {
		for(int column = 0; column<length; column ++) {
			System.out.print("*" + "\t");
		}
		System.out.println();
		System.out.println();
	}
	
	
	
}